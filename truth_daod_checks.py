import os, uproot as up

path = "/eos/home-j/jagrundy/hh4b/DAODs/mc16_13TeV.502970.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1.deriv.DAOD_EXOT8.e8263_s3126_r10201_p4089"
fname = "DAOD_EXOT8.24331813._000004.pool.root.1"

f = up.open(os.path.join(path, fname))

for k in f["CollectionTree"].keys():
    if "Particles" in k:
        print(k)

# print(f["CollectionTree"]["TruthBosonsWithDecayParticlesAux./TruthBosonsWithDecayParticlesAux.pdgId"].array()[0])
# print(f["CollectionTree"]["TruthBosonsWithDecayParticlesAux./TruthBosonsWithDecayParticlesAux.pdgId"].array()[1])
# print(f["CollectionTree"]["TruthBosonsWithDecayParticlesAux./TruthBosonsWithDecayParticlesAux.pdgId"].array()[2])