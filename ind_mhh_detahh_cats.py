import numpy as np, sys, matplotlib.pyplot as plt

sys.path.append("/afs/cern.ch/work/j/jagrundy/public/hh4b/hh4b-plots/")
from hh4b_plots.hh4b_plots import hh4b_plots

def ind_cat_plots(data_files, sm_files, kl10_files, k2v0_files, yr, target_reg, detahh_cut_str,
               output_dir='',output_fname='', ind_scale=True):
    '''
    '''

    if detahh_cut_str == "dEta_hh<1.5":
        kwargs.update({"bins" : bins_lt})
        cattex=", $\Delta\eta_{HH} < 1.5$"

    if detahh_cut_str == "dEta_hh>=1.5":
        kwargs.update({"bins" : bins_gt})
        cattex=", $\Delta\eta_{HH} \geq 1.5$"

    print(f"Making plots for {tree}, {target_reg}, Xwt > {Xwt_cut}")

    #Some preamble stuff to handle various inputs, set up all columns we want
    if yr == 'all':
        yrs = ['16', '17', '18']
    else:
        yrs = [yr]

    if isVBF:
        VBFflag = 'pass_vbf_sel'
    else:
        VBFflag = '~pass_vbf_sel'

    columns = ['ntag', 'm_hh', 'm_h1', 'm_h2', 'dEta_hh', 'X_wt_tag', 'X_hh',
               'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']
       
    cut_str = f'X_wt_tag > {Xwt_cut}' +"&"+ VBFflag +"&"+ detahh_cut_str
      
    #The meat of the thing: set up an hh4b_plots object
    plotter_data = hh4b_plots()

    #Load in data
    plotter_data.load(data_files, tree, yrs, columns=columns, target_reg=target_reg)

    #Set up labeling
    plotter_data.set_labels(NNT_tag=NNT_tag, isggF=(not isVBF), yr=yr, target_reg=target_reg, cattext=cattex)

    #Set up background. If no_rw, we don't want bootstrap. This is enforced internally
    #as well, but done explicitly here for transparency
    plotter_data.set_bkgd(target_reg=target_reg, extra_sel=cut_str,
                          yr=yr, with_bootstrap=(not no_rw), with_poisson=True, 
                          with_shape=with_shape, stack=False)

    bkgd_label='Reweighted 2b Data'
    
    # Do the same thing to load in and categorize the signal

    bkg_sum_wghts = sum(plotter_data.bkgd.nom_weights)

    # SM
    plotter_sig = hh4b_plots()
    plotter_sig.load(sm_files, tree, yrs, columns=columns, target_reg=target_reg, isMC=True, withNNweights=False)
    plotter_sig.set_target(target_reg=target_reg, extra_sel=cut_str, yr=yr)

    if ind_scale:
        alpha_sm = round(0.75*bkg_sum_wghts/sum(plotter_sig.target.nom_weights))
        alpha_sm_lab = f'{alpha_sm} x SM HH'
    else:
        alpha_sm = 839 #1.0
        alpha_sm_lab = f'{alpha_sm} x SM HH'

    plotter_sig.target.scale(alpha_sm)
    
    # kl = 10
    plotter_kl10 = hh4b_plots()
    plotter_kl10.load(kl10_files, tree, yrs, columns=columns, target_reg=target_reg, 
                     isMC=True, withNNweights=False)
    plotter_kl10.set_target(target_reg=target_reg, extra_sel =cut_str, yr=yr)
    
    if ind_scale:
        alpha_kl10 = round(0.75*bkg_sum_wghts/sum(plotter_kl10.target.nom_weights))
        alpha_kl10_lab = f'{alpha_kl10} x $\kappa_\lambda = 10$ HH'
    else:
        alpha_kl10 = 13 #1.0
        alpha_kl10_lab = f'{alpha_kl10} x $\kappa_\lambda = 10$ HH'

    plotter_kl10.target.scale(alpha_kl10)

    # k2v = 0
    plotter_k2v0 = hh4b_plots()
    plotter_k2v0.load(k2v0_files, tree, yrs, columns=columns, target_reg=target_reg, 
                     isMC=True, withNNweights=False)
    plotter_k2v0.set_target(target_reg=target_reg, extra_sel =cut_str, yr=yr)

    if ind_scale:
        alpha_k2v0 = round(0.75*bkg_sum_wghts/sum(plotter_k2v0.target.nom_weights))
        alpha_k2v0_lab = f'{alpha_k2v0}'+' x $\kappa_{2V} = 0$ HH'
    else:
        alpha_k2v0 = 12 #1.0
        alpha_k2v0_lab = f'{alpha_k2v0}'+' x $\kappa_{2V} = 0$ HH'

    plotter_k2v0.target.scale(alpha_k2v0)

    # Plot
    plotter_data.hist1d(col, bkgd_label=bkgd_label, fc='hh:darkyellow', alpha=0.9, ec='k',
                            xmin=.015, ymax=.8, **kwargs)

    ax = plotter_data.ax

    plotter_sig.hist1d(col, data_label=alpha_sm_lab, data_style="step",
                           reuse_ax=ax, c=c, **kwargs) 

    plotter_kl10.hist1d(col, data_label=alpha_kl10_lab, data_style="step",
                           reuse_ax=ax,  c=ckl10, **kwargs) 

    plotter_k2v0.hist1d(col, data_label=alpha_k2v0_lab, data_style="step",
                           reuse_ax=ax,  c=ck2v0, **kwargs) 

    # Draw the significance subpanel
    b = plotter_data.bkgd.h_m_hh.values
    s  = plotter_sig.target.h_m_hh.values
    kl10  = plotter_kl10.target.h_m_hh.values
    k2v0  = plotter_k2v0.target.h_m_hh.values

    print(dir(plotter_data))

    ax[1].plot(plotter_data.bin_midpoints, s/(np.sqrt(b)), color=c,marker='o', lw=0)    
    ax[1].plot(plotter_data.bin_midpoints, kl10/(np.sqrt(b)), color=ckl10,marker='o', lw=0)
    ax[1].plot(plotter_data.bin_midpoints, k2v0/(np.sqrt(b)), color=ck2v0,marker='o', lw=0)      

    ax[1].set_xlabel('$m_{HH}$ [GeV]',  horizontalalignment='right', x=.99, fontsize=18)

    if ind_scale:
        ax[1].set_ylabel(r'$\alpha$ x $S$ / $\sqrt{B}$', fontsize=18)
    else:
        ax[1].set_ylabel(r'$S$ / $\sqrt{B}$', fontsize=18)

    ax[0].set_ylabel(f'Entries / {ref_val} GeV',ha='right',y=1, fontsize=18)

    # Reset the x-spacing for the yticklabels on the subpanel
    for label in ax[1].yaxis.get_majorticklabels():
        label.set_x(0)
    
    # I wanted to resort the legend such that the bkg + bkg error were together (+ first on the plot)
    handles, labels = ax[0].get_legend_handles_labels()
    handles, labels = zip(*[(handles[i],labels[i]) for i in [0,3,1,2]])

    ax[0].legend(handles, labels, fontsize=16, loc='upper right', ncol=1)

    # Adjust to take out some of the white space on the top
    _,ymax = ax[0].get_ylim()
    ax[0].set_ylim(0, ymax) # .78 works nicely if we don't divide by bin width
        
    # Save the figure
    if output_dir:

        if ind_scale:
            plt.savefig(f'{output_dir}/{output_fname}_indi_scaling.png',bbox_inches='tight')
        else:
            plt.savefig(f'{output_dir}/{output_fname}_same_scaling.png',bbox_inches='tight')

    plt.show()

#

if __name__ ==  "__main__":

    tree='sig'
    target_reg='4b'
    Xwt_cut=1.5

    NNT_tag='sig:cryptotuples-MAY21, bkg:correlated_vars_basic_set_100b (Xhh45, crypto)'

    yr='all'
    mcs = ['a','d','e']
    isVBF=True

    no_rw=False
    with_shape=True
    ind_scale=True

    cat_label="VBF_2_dEta"

    #

    print(f"Making plots for {tree}, {target_reg}, Xwt > {Xwt_cut}")

    #Some preamble stuff to handle various inputs, set up all columns we want
    if yr == 'all':
        yrs = ['16', '17', '18']
    else:
        yrs = [yr]

    if isVBF:
        VBFflag = ' & pass_vbf_sel'
    else:
        VBFflag = ' & ~pass_vbf_sel'

    data_label=f'{target_reg} Data'

    bins_lt = np.array([280, 310, 340, 375, 410, 450, 495, 545, 600, 660, 725, 800, 880, 965])
    bins_gt = np.array([290, 315, 345, 375, 410, 445, 485, 530, 580, 630, 685, 750, 815, 890, 970, 1055, 1150, 1255, 1370, 1490])

    col='m_hh'

    c='hh:darkpink'
    ckl10 = 'hh:medturquoise'
    ck2v0 = 'hh:darkblue'

    ref_val = -1.0

    kwargs = {  'underflow':True,
                'overflow':True,
                #'uoflow_append':True,
                'withRatio':True, 
                'withPulls':False, 
                'chi2':False,
                'store_hist':True,
                'ref_val':ref_val}

    output_dir = 'plots/'

    path_to_data = "/eos/user/m/mczurylo/hh4b/output_May21_debug/correlated_vars_basic_set_100b/"

    data_filess = [f'{path_to_data}/data{yr}_Xhh_45_NN_100_bootstraps.root' for yr in yrs]

    path_to_mc = "/eos/user/h/hartman/hh4b/NNTs/cryptotuples-MAY21/VBF/"

    sm_files = [f'{path_to_mc}/vbf_502970_mc16{mc}.root' for mc in mcs]
    kl10_files = [f'{path_to_mc}/vbf_502978_mc16{mc}.root' for mc in mcs]
    k2v0_files = [f'{path_to_mc}/vbf_502971_mc16{mc}.root' for mc in mcs]

    output_fname = f'm_hh_{cat_label}_VBF_{yr}_{target_reg}_new'

    ind_cat_plots(data_filess, sm_files, kl10_files, k2v0_files, yr, "4b", "dEta_hh>=1.5", output_dir, output_fname+"_detahh_gt_1p5", ind_scale=ind_scale)
    ind_cat_plots(data_filess, sm_files, kl10_files, k2v0_files, yr, "4b", "dEta_hh<1.5", output_dir, output_fname+"_detahh_lt_1p5", ind_scale=ind_scale)