import yaml, json, pandas as pd, matplotlib.pyplot as plt, numpy as np
import atlas_mpl_style as ampl, argparse
from scipy.optimize import curve_fit
import sys

def quadratic(x, a, b, c):
    return a*x*x + b*x + c

def fit_quad(xdata, ydata):

    paras, _ = curve_fit(quadratic, xdata, ydata)

    return paras

def get_bkg_yields(wksp):

    ggf_bkg, vbf_bkg = 0, 0, #chan_samp
    for chan in wksp["channels"]:
        for samp in chan["samples"]:
            if "bkg" in samp["name"]:
                if "ggf" in chan["name"]:
                    ggf_bkg += sum(samp['data'])
                if "vbf" in chan["name"]:
                    vbf_bkg += sum(samp['data'])

    yields_df = pd.DataFrame({"ggf_bkg" : ggf_bkg, 
                              "vbf_bkg" : vbf_bkg}, index=[0])

    return yields_df

def get_sig_yields(wksp):

    ggf_ggf, ggf_vbf, vbf_ggf, vbf_vbf = 0, 0, 0, 0 #chan_samp
    for chan in wksp["channels"]:
        for samp in chan["samples"]:
            if "Sig" in samp["name"]:
                if "ggf" in chan["name"] and "ggfSig" in samp["name"]:
                    ggf_ggf += sum(samp['data'])
                if "ggf" in chan["name"] and "vbfSig" in samp["name"]:
                    ggf_vbf += sum(samp['data'])
                if "vbf" in chan["name"] and "ggfSig" in samp["name"]:
                    vbf_ggf += sum(samp['data'])
                if "vbf" in chan["name"] and "vbfSig" in samp["name"]:
                    vbf_vbf += sum(samp['data'])

    yields_df = pd.DataFrame({"ggf_ggf" : ggf_ggf, 
                              "ggf_vbf" : ggf_vbf,
                              "vbf_ggf" : vbf_ggf,
                              "vbf_vbf" : vbf_vbf}, index=[0])

    return yields_df

def open_workspaces(wksp_yaml):

    wksp_dir = wksp_yaml["dir"]
    example_f = wksp_yaml["example_file"]

    if c_str == "k2v":
        coups = [-4., -3. , -2. , -1.8, -1.6, -1.4, -1.2, -1. , -0.8, -0.6, -0.4, -0.2,  0. ,  0.2, 0.4,  0.6,  0.8,  1. ,  1.2,  1.4,  1.6,  1.8,  2. ,  2.2,  2.4, 2.6,  2.8, 3. , 4., 5. , 6. ]
    elif c_str == "kl":
        coups = np.linspace(-20., 20., 41)
    
    sig_yeilds_df, bkg_yeilds_df = pd.DataFrame({}), pd.DataFrame({})
    for c in coups:
        infile = example_f.replace(f"{c_str}_1.00", f"{c_str}_{c}0")
        wksp = json.load(open(wksp_dir+infile))
        sig_yeilds_df = sig_yeilds_df.append(get_sig_yields(wksp), ignore_index=True)
        bkg_yeilds_df = bkg_yeilds_df.append(get_bkg_yields(wksp), ignore_index=True)

    sig_yeilds_df[c_str], bkg_yeilds_df[c_str] = coups, coups

    return sig_yeilds_df, bkg_yeilds_df

def make_plots(sig_yields_df, bkg_yields_df):

    ampl.use_atlas_style(atlasLabel='Thesis')

    fig, ax = plt.subplots()

    if c_str == "k2v":
        xlim=[-5, 7]
        c_latex = "$\kappa_{2V}$"
    elif c_str == "kl":
        xlim=[-21, 21]
        c_latex = "$\kappa_{\lambda}$"

    coups = sig_yields_df[c_str]
    fine_coups = np.linspace(xlim[0], xlim[1], 1000)

    if debug_quad:
        ax.plot(coups, sig_yields_df["ggf_vbf"], "-", color="#36B1BF", label="VBF MC (ggF channel)")
        ax.plot(coups, sig_yields_df["vbf_vbf"], "--", color="#36B1BF", label="VBF MC (VBF channel)")
        if c_str == "kl":
            ax.plot(coups, sig_yields_df["ggf_ggf"], "*", color="#F2385A", label="ggF MC (ggF channel)")
            ax.plot(coups, sig_yields_df["vbf_ggf"], "*", color="#F2385A", label="ggF MC (VBF channel)")
        
    paras_ggf_vbf = fit_quad(coups, sig_yields_df["ggf_vbf"])
    paras_vbf_vbf = fit_quad(coups, sig_yields_df["vbf_vbf"])
    paras_ggf_ggf = fit_quad(coups, sig_yields_df["ggf_ggf"])
    paras_vbf_ggf = fit_quad(coups, sig_yields_df["vbf_ggf"])

    ax.plot(fine_coups, quadratic(fine_coups, *paras_ggf_vbf), "-", color="#36B1BF", label="VBF MC (ggF channel)")
    ax.plot(fine_coups, quadratic(fine_coups, *paras_vbf_vbf), "--", color="#36B1BF", label="VBF MC (VBF channel)")
    if c_str == "kl":
        ax.plot(fine_coups, quadratic(fine_coups, *paras_ggf_ggf), "-", color="#F2385A", label="ggF MC (ggF channel)")
        ax.plot(fine_coups, quadratic(fine_coups, *paras_vbf_ggf), "--", color="#F2385A", label="ggF MC (VBF channel)")

    ax.set(yscale="log", xlim=xlim, ylim=[0.1, 1e5])
    ax.legend()

    ampl.plot.draw_atlas_label(0.05, 0.95, ax=ax, status='final', energy="13 TeV", lumi=126, fontsize=24)
    ampl.set_xlabel(c_latex, fontsize=24) #\lambda
    ampl.set_ylabel("Signal Yield", fontsize=24)

    plt.savefig(f"signal_yield_versus_{c_str}.pdf", facecolor="white")

    fig, ax = plt.subplots()

    if debug_quad:
        ax.plot(coups, sig_yields_df["ggf_vbf"]/np.sqrt(bkg_yields_df["ggf_bkg"]), "*", color="#36B1BF", label="VBF MC (ggF channel)")
        ax.plot(coups, sig_yields_df["vbf_vbf"]/np.sqrt(bkg_yields_df["vbf_bkg"]), "*", color="#36B1BF", label="VBF MC (VBF channel)")
        if c_str == "kl":
            ax.plot(coups, sig_yields_df["ggf_ggf"]/np.sqrt(bkg_yields_df["ggf_bkg"]), "*", color="#F2385A", label="ggF MC (ggF channel)")
            ax.plot(coups, sig_yields_df["vbf_ggf"]/np.sqrt(bkg_yields_df["vbf_bkg"]), "*", color="#F2385A", label="ggF MC (VBF channel)")

    paras_ggf_vbf = fit_quad(coups, sig_yields_df["ggf_vbf"]/np.sqrt(bkg_yields_df["ggf_bkg"]))
    paras_vbf_vbf = fit_quad(coups, sig_yields_df["vbf_vbf"]/np.sqrt(bkg_yields_df["vbf_bkg"]))
    paras_ggf_ggf = fit_quad(coups, sig_yields_df["ggf_ggf"]/np.sqrt(bkg_yields_df["ggf_bkg"]))
    paras_vbf_ggf = fit_quad(coups, sig_yields_df["vbf_ggf"]/np.sqrt(bkg_yields_df["vbf_bkg"]))

    ax.plot(fine_coups, quadratic(fine_coups, *paras_ggf_vbf), "-", color="#36B1BF", label="VBF MC (ggF channel)")
    ax.plot(fine_coups, quadratic(fine_coups, *paras_vbf_vbf), "--", color="#36B1BF", label="VBF MC (VBF channel)")
    if c_str == "kl":
        ax.plot(fine_coups, quadratic(fine_coups, *paras_ggf_ggf), "-", color="#F2385A", label="ggF MC (ggF channel)")
        ax.plot(fine_coups, quadratic(fine_coups, *paras_vbf_ggf), "--", color="#F2385A", label="ggF MC (VBF channel)")


    ax.set(yscale="log", xlim=xlim, ylim=[0.001, 1e3])
    ax.legend()

    ampl.plot.draw_atlas_label(0.05, 0.95, ax=ax, status='final', energy="13 TeV", lumi=126, fontsize=24)
    ampl.set_xlabel(c_latex, fontsize=24)
    ampl.set_ylabel("S/$\sqrt{B}$", fontsize=24)

    plt.savefig(f"s_over_sqrtb_versus_{c_str}.pdf", facecolor="white")

if __name__ == "__main__":

    debug_quad = False

    xsec = pd.read_csv("/afs/cern.ch/user/j/jagrundy/public/hh4b/hh4b-resolved-reconstruction/rdf-utils/mc-config/xsec.tsv", sep='\t', 
                   header=None,
                   names=['Name', 'xs', 'feff', 'k', '2', '3','4'], index_col=0)

    parser = argparse.ArgumentParser()

    parser.add_argument("-c",
                        "--coupling-modifier",
                        type=str,
                        required=True,
                        choices=["kl", "k2v"],
                        help="Coupling modifier for the x-axis.")

    args = parser.parse_args()

    c_str = args.coupling_modifier

    wksp_yaml = yaml.safe_load(open("input_workspaces.yaml"))

    sig_yields_df, bkg_yields_df = open_workspaces(wksp_yaml)

    make_plots(sig_yields_df, bkg_yields_df)