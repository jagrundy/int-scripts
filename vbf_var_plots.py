import yaml, numpy as np, matplotlib.pyplot as plt, sys

sys.path.append("/afs/cern.ch/user/j/jagrundy/public/hh4b/hh4b-plots/")
from hh4b_plots.hh4b_plots import hh4b_plots

def vbf_var_plots(infiles_dict, incl_vbf_mc, incl_ggf_mc, incl_vbf_data, incl_ggf_data, scale_to):

    # Plotters

    sig_plotters, data_plotters = [], []

    if incl_vbf_mc:
        vbf_mc_dir = infiles_dict["mc"]["vbf"]["dir"]
        vbf_sig_pltr_sm = signal_plotter(vbf_mc_dir, list(infiles_dict["mc"]["vbf"]["files"]["sm"].values()), "pink")
        vbf_sig_pltr_kl10 = signal_plotter(vbf_mc_dir, list(infiles_dict["mc"]["vbf"]["files"]["kl10"].values()), "blue")
        vbf_sig_pltr_k2v0 = signal_plotter(vbf_mc_dir, list(infiles_dict["mc"]["vbf"]["files"]["k2v0"].values()), "turq")

        sig_plotters += [vbf_sig_pltr_sm, vbf_sig_pltr_kl10, vbf_sig_pltr_k2v0]

    if incl_ggf_mc:
        ggf_mc_dir = infiles_dict["mc"]["ggf"]["dir"]
        ggf_sig_pltr_sm = signal_plotter(ggf_mc_dir, list(infiles_dict["mc"]["ggf"]["files"]["sm"].values()), "pink")
        ggf_sig_pltr_kl10 = signal_plotter(ggf_mc_dir, list(infiles_dict["mc"]["ggf"]["files"]["kl10"].values()), "blue")

        sig_plotters += [ggf_sig_pltr_sm, ggf_sig_pltr_kl10]

    if incl_vbf_data:
        vbf_data_dir = infiles_dict["data"]["vbf"]["dir"]
        vbf_data_pltr = data_plotter(vbf_data_dir, list(infiles_dict["data"]["vbf"]["files"].values()))

        data_plotters += [vbf_data_pltr]

    if incl_ggf_data:
        ggf_data_dir = infiles_dict["data"]["ggf"]["dir"]
        ggf_data_pltr = data_plotter(ggf_data_dir, list(infiles_dict["data"]["vbf"]["files"].values()))

        data_plotters += [ggf_data_pltr]

    # Scalings 

    if scale_to == "100":
        scalings = [
            round(100.0/sum(sig_pltr.target.nom_weights))
            for sig_pltr in sig_plotters
        ]

    if scale_to == "bkg":
        scalings = [
            round(sum(vbf_data_pltr.bkgd.nom_weights)/sum(sig_pltr.target.nom_weights))
            for sig_pltr in sig_plotters
        ]
    
    scalings += len(data_plotters)*[1.0]
    scalings_cp = scalings

    # Legend labels

    leg_labs = []
    if incl_vbf_mc:
        leg_labs += [f"{scalings_cp.pop(0)} x 4b SM",
                     f"{scalings_cp.pop(0)}"+" x 4b $\kappa_{\lambda} = 10$",
                     f"{scalings_cp.pop(0)}"+" x 4b $\kappa_{2V} = 0$"]

    if incl_ggf_mc:
        leg_labs += [f"{scalings_cp.pop(0)} x 4b SM",
                     f"{scalings_cp.pop(0)}"+" x 4b $\kappa_{\lambda} = 10$"]
    
    if incl_vbf_data:
        leg_labs += ["RWed 2b data"]

    if incl_ggf_data:
        leg_labs += ["RWed 2b data"]

    # Plotting

    sig_plotters[0].set_labels(NNT_tag=labels["NNT_tag"], isggF=False, yr=yr, label_color='k',
                           cattext=f', {labels["Xwt_cut_lab"]}')

    for var in vars:

        hists = []

        hists += [
            getattr(sig_pltr.target, f"h_{var}") 
            for sig_pltr in sig_plotters
            ]

        hists += [
            getattr(data_pltr.bkgd, f"h_{var}") 
            for data_pltr in data_plotters
            ]

        sig_plotters[0].plot(hists, labels=leg_labs, scales=scalings)

        ax = sig_plotters[0].ax
        ax.set_ylabel("Events", ha='right', y=1.0)
        ax.set_xlabel(vars_labels[var])
        
        _, ymax = ax.get_ylim()
        ax.plot([vars_cuts[var]]*2,[0,.63*ymax], color='mediumpurple', lw=2)

        out_file_name = f"{var}"
        if incl_vbf_mc: out_file_name += "_vbf_hh_mc"
        if incl_ggf_mc: out_file_name += "_ggf_hh_mc"
        if incl_vbf_data: out_file_name += "_vbf_hh_RWed2b_bkg"
        if incl_ggf_data: out_file_name += "_ggf_hh_RWed2b_bkg"
        out_file_name += ".png"

        plt.savefig(out_file_name, bbox_inches='tight')

def signal_plotter(dr, files, colour):

    files = [dr+f for f in files]

    colours = {
        "pink": "hh:darkpink",
        "blue" : "hh:darkblue",
        "turq" : "hh:medturquoise"
    }

    sig_pltr = hh4b_plots()

    sig_pltr.load(files, tree, yrs, columns=columns, target_reg=target_reg, isMC=True, withNNweights=False)

    sig_pltr.set_target(target_reg=target_reg, extra_sel=f"{vbf_flag}&{extra_cut}", yr=yr)

    for var in vars:
        sig_pltr.target.make_hist1d(var,
                                    bins = vars_bins[var],
                                    style_kw = {'ec': colours[colour], 'fc' : 'None', 'lw' : 2},
                                    showError='',)

    return sig_pltr

def data_plotter(dr, files):

    files = [dr+f for f in files]

    data_pltr = hh4b_plots()

    data_pltr.load(files, tree, yrs, columns=columns, target_reg=target_reg, isMC=False, withNNweights=True)

    data_pltr.set_bkgd(target_reg=target_reg, extra_sel=f"{vbf_flag}&{extra_cut}", yr=yr,
                        with_bootstrap=False, with_poisson=False, with_shape=False, stack=False)

    for var in vars:
        data_pltr.bkgd.make_hist1d(var,
                                   bins = vars_bins[var],
                                   style_kw = {'fc':'hh:darkyellow', 'alpha':0.9, 'ec':'k', 'lw':1},
                                   showError='',)
    
    return data_pltr

if __name__ ==  "__main__":

    yaml_file = open("input_files.yaml", "r")

    infiles_dict = yaml.safe_load(yaml_file)

    vars = ["vbf_mjj",
            "vbf_dEtajj",
            "vbf_pTvecsum"]

    vars_labels = {
        "vbf_mjj"      : "$m_{jj}$ [GeV]",
        "vbf_dEtajj"   : "|$\Delta\eta_{jj}|$",
        "vbf_pTvecsum" : "$(\overline{p}_{HH} + \overline{p}_{jj})_{T}$ [GeV]"
    }

    vars_bins = {
        "vbf_mjj"      : np.linspace(800, 4000, 17),
        "vbf_dEtajj"   : np.linspace(2.0, 10.0, 17),
        "vbf_pTvecsum" : np.linspace(0, 75, 16)
    }

    vars_cuts = {
        "vbf_mjj"      : 1000.0,
        "vbf_dEtajj"   : 3.0,
        "vbf_pTvecsum" : 65.0
    }

    labels = {
        "NNT_tag"     : "sig:cryptotuples-MAY21, bkg:correlated_vars_basic_set_100b (Xhh45, crypto)",
        "Xwt_cut_lab" : "post-$X_{Wt}$ cut"
    }

    # Settings
    tree = "sig"
    yr = "all"
    yrs = ["16", "17", "18"]
    columns = ["vbf_mjj", "vbf_dEtajj", "vbf_pTvecsum", "X_wt_tag", "kinematic_region"]
    target_reg = "4b"
    vbf_flag = "pass_vbf_sel"
    extra_cut = "X_wt_tag > 1.5"

    vbf_var_plots(infiles_dict,
                  incl_vbf_mc=True,
                  incl_vbf_data=True,
                  incl_ggf_mc=False,
                  incl_ggf_data=False,
                  scale_to="bkg")