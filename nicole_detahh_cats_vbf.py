import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from matplotlib.ticker import MultipleLocator
from hh4b_plots.hh4b_plots import hh4b_plots

# use branch incl_ttbar

# Set up options

fourb_or_bkg = "bkg" #"fourb" or "bkg"

if fourb_or_bkg == "fourb":
    tree='fullmassplane'
    extra_cut = 'X_wt_tag > 0.0'
    Xwt_cut_lab = 'pre-$X_{Wt}$ cut'
    use_bkg_wghts=False
else:
    tree='sig'
    extra_cut = 'X_wt_tag > 1.5'
    Xwt_cut_lab = 'post-$X_{Wt}$ cut'
    use_bkg_wghts=True

target_reg='4b'
yr='all'
yrs=['16', '17', '18']
mcs = ['a','d','e']
isVBF=True

if isVBF:
    VBFflag = 'pass_vbf_sel'
else:
    VBFflag = '~pass_vbf_sel'

columns = ['ntag', 'dEta_hh', 'm_hh', 'X_wt_tag', 'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

output_dir = 'plots/'
NNT_tag='sig:cryptotuples-MAY21, bkg:correlated_vars_basic_set_100b (Xhh45, crypto)'

lw=4
bins=np.linspace(0,5,51)

# Set up the plotter objects

# Signal

path_to_mc = "/eos/user/h/hartman/hh4b/NNTs/cryptotuples-MAY21/VBF/"

files_sm = [f'{path_to_mc}/vbf_502970_mc16{mc}.root' for mc in mcs]
files_kl10 = [f'{path_to_mc}/vbf_502978_mc16{mc}.root' for mc in mcs]
files_k2v0 = [f'{path_to_mc}/vbf_502971_mc16{mc}.root' for mc in mcs]

sig_plotter_sm = hh4b_plots()
sig_plotter_sm.load(files_sm, tree, yrs, columns=columns, target_reg=target_reg, isMC=True, withNNweights=False)

sig_plotter_kl10 = hh4b_plots()
sig_plotter_kl10.load(files_kl10, tree, yrs, columns=columns, target_reg=target_reg, isMC=True, withNNweights=False)

sig_plotter_k2v0 = hh4b_plots()
sig_plotter_k2v0.load(files_k2v0, tree, yrs, columns=columns, target_reg=target_reg, isMC=True, withNNweights=False)

# Data

path_to_data = "/eos/user/m/mczurylo/hh4b/output_May21_debug/correlated_vars_basic_set_100b/"

nom_dat_files = [f'{path_to_data}/data{yr}_Xhh_45_NN_100_bootstraps.root' for yr in yrs]

dat_plotter = hh4b_plots()

dat_plotter.load(nom_dat_files, tree, yrs, columns=columns+['kinematic_region'],
                 target_reg=target_reg, isMC=False, withNNweights=use_bkg_wghts)

# Make the histograms

# Sig 4b
sig_plotter_sm.set_target(target_reg=target_reg, extra_sel=f"{VBFflag}&{extra_cut}", yr=yr)
sig_plotter_sm.target.make_hist1d('dEta_hh',
                               bins = bins,
                               style_kw = {'ec': 'hh:darkpink',
                                           'fc' : 'None',
                                           'lw' : lw},
                               showError='',)

sig_plotter_kl10.set_target(target_reg=target_reg, extra_sel=f"{VBFflag}&{extra_cut}", yr=yr)
sig_plotter_kl10.target.make_hist1d('dEta_hh',
                               bins = bins,
                               style_kw = {'ec': 'hh:medturquoise',
                                           'fc' : 'None',
                                           'lw' : lw},
                               showError='',)

sig_plotter_k2v0.set_target(target_reg=target_reg, extra_sel=f"{VBFflag}&{extra_cut}", yr=yr)
sig_plotter_k2v0.target.make_hist1d('dEta_hh',
                               bins = bins,
                               style_kw = {'ec': 'hh:darkblue',
                                           'fc' : 'None',
                                           'lw' : lw},
                               showError='',)

# 4b

if fourb_or_bkg == "fourb":
    dat_plotter.set_target(target_reg=target_reg, extra_sel=f"{VBFflag}&{extra_cut}&(kinematic_region != 0)", yr=yr)

    dat_plotter.target.make_hist1d('dEta_hh',
                                bins = bins,
                                style_kw = {'lw':lw, 'ls':'dotted', 'fc':'None', 'ec':'k'},
                                showError='',
                                )
else:
    dat_plotter.set_bkgd(target_reg=target_reg, extra_sel=f"{VBFflag}&{extra_cut}", yr=yr,
                        with_bootstrap=False, with_poisson=False, with_shape=False, stack=False)

    dat_plotter.bkgd.make_hist1d('dEta_hh',
                                bins = bins,
                                style_kw = {'fc':'hh:darkyellow', 'alpha':0.9, 'ec':'k', 'lw':1},
                                showError='',
                                )

#Draw and save the plots
dat_plotter.set_labels(NNT_tag=NNT_tag, isggF=(not isVBF), yr=yr, label_color='k',
                           cattext=f', {Xwt_cut_lab}')

hists = [ sig_plotter_sm.target.h_dEta_hh ,
          sig_plotter_kl10.target.h_dEta_hh ,
          sig_plotter_k2v0.target.h_dEta_hh]

if fourb_or_bkg == "fourb":
    s_sm, s_kl10, s_k2v0 = 25000, 375, 750
else:
    s_sm, s_kl10, s_k2v0 = 1250, 20, 20

labels = [f'{s_sm} x 4b SM',
          f'{s_kl10}'+' x 4b $\kappa_{\lambda} = 10$',
          f'{s_k2v0}'+' x 4b $\kappa_{2V} = 0$']

if fourb_or_bkg == "fourb":
    hists.append(dat_plotter.target.h_dEta_hh)
    labels.append('4b data (blinded)')
else:
    hists.append(dat_plotter.bkgd.h_dEta_hh)
    labels.append('RWed 2b data')

scales = [s_sm, s_kl10, s_k2v0, 1]

dat_plotter.plot(hists, labels=labels, scales=scales)

# Add a dashed line for our SR defining cut
ax = dat_plotter.ax
ymin, ymax = ax.get_ylim()
ax.plot([1.5]*2,[0,.65*ymax],color='mediumpurple',lw=lw)

ax.set_ylabel('Events / 0.1', ha='right', y=1.0, fontsize=18)

ax.xaxis.set_minor_locator(MultipleLocator(0.1))
ax.xaxis.set_major_locator(MultipleLocator(0.5))

if fourb_or_bkg == "fourb":
    plt.savefig(f'{output_dir}/dEta_hh_VBF_fmp_sig_4bdata_allyears.png',bbox_inches='tight')
else:
    plt.savefig(f'{output_dir}/dEta_hh_VBF_sr_sig_bkg_allyears.png',bbox_inches='tight')