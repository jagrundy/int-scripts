import uproot as up, os, matplotlib.pyplot as plt, numpy as np, atlas_mpl_style as ampl

def make_plots(dsids, tag):

    ampl.use_atlas_style(atlasLabel='ATLAS')
    ampl.set_color_cycle(pal="HH")

    fig, ax = plt.subplots()

    ampl.plot.draw_atlas_label(0.05, 0.95, ax=ax, status='int', energy="13 TeV", fontsize=24)

    cols = ["hh:darkpink", "hh:darkyellow", "hh:medturquoise"]

    for dsid, col in zip(dsids, cols):
        fname = f"{dsid}.root"
        mnt = up.open(os.path.join(path, fname))["XhhMiniNtuple;1"]
        truth_mhh = np.array(mnt["truth_mHH"].array()) / 1e3
        weights = np.array(mnt["mcEventWeight"].array()) * np.array(mnt["weight_pileup"].array())
        #ax.hist(truth_mhh, bins, label=f"{dsid_latex_dict[dsid]}", )

        if normalise:
            h_tmhh, bin_edges = np.histogram(truth_mhh, bins, weights=weights, density=1.0)
        else:
            h_tmhh, bin_edges = np.histogram(truth_mhh, bins, weights=weights)

        ampl.plot.plot_1d(f"{dsid_latex_dict[dsid]}", bin_edges, h_tmhh, lw=4, color=col)

    ax.margins(y=0.25)

    ampl.set_xlabel("Truth $m_{HH}$ [GeV]", fontsize=24)
    if normalise:
        ampl.set_ylabel("Events Normalised to Unity", fontsize=24)
        ax.set(xlim=[200, 1550])
        tag += "_normed"
    else:
        ampl.set_ylabel("Events", fontsize=24)
        ax.set(xlim=[200, 1550], yscale="log")

    ax.legend()

    plt.savefig(f"vbf_mod_vars_truth_mhh_{tag}.png", facecolor="white")

if __name__ == "__main__":
    
    dsid_latex_dict = {
        502970 : "SM",
        502971 : "$\kappa_{2V} = 0$",
        502975 : "$\kappa_{2V} = 3$",
        502976 : "$\kappa_{\lambda} = 0$",
        502978 : "$\kappa_{\lambda} = 10$"
    }

    path = "/eos/home-j/jagrundy/hh4b/MiniNTuples/user.arelycg.HH4B.5029XX.VBF_nonres2021.MC16e-2018.AB21.2.163.MAY21-NR-0.full_nonres_MiniNTuple"

    k2v_dsids = [502970, 502971, 502975]
    kl_dsids = [502970, 502976, 502978]

    bins = np.linspace(250, 1500, 26)

    normalise = False
    
    make_plots(kl_dsids, "kl")
    make_plots(k2v_dsids, "k2v")